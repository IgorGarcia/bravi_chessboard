def predictMoves(
  totalMoves: dict,
  maxColumns: int = 8,
  maxRows: int = 8,
  turns: int = 2,
  actualTurn: int = 1
):
  positions = totalMoves[actualTurn - 1]
  movesRound = []
  for position in positions:
    row, column = position
    moves = []
    for line in range(-2, 3):
      if line and (row + line) >= 0 and (row + line) < maxRows:
        position_aside = 3 - abs(line)
        if (column - position_aside) >= 0:
          moves.append((row+line, column - position_aside))
        if (column + position_aside) < maxColumns:
          moves.append((row+line, column+position_aside))

    movesRound += moves
  # Pass to a set to remove the duplicates values
  totalMoves[actualTurn] = list(set(movesRound))

  if int(turns) == int(actualTurn):
    return totalMoves

  return predictMoves(
    totalMoves,
    maxColumns,
    maxRows,
    turns,
    actualTurn=actualTurn+1
  )
