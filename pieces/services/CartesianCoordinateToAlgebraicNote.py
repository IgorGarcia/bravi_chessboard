
from typing import Tuple

def CartesianCoordinateToAlgebraicNote(coordinate: (list or Tuple)):
  '''
  CartesianCoordinateToAlgebraicNote(0, 2)
  >>>C1
  CartesianCoordinateToAlgebraicNote(4, 2)
  >>>C4
  '''
  x, y = coordinate
  letter = chr(y + 65)
  return "{0}{1}".format( letter, x+1)