from django.http import JsonResponse
from django.http.request import HttpRequest
from django.views.generic.base import View
from ..models import Color


class ColorsView(View):
  def dispatch(self, request: HttpRequest, id=None):
    return self.index(id)

  def index(self, index=None):
    try:
      if index:
        colors = Color.objects.filter(pk=index).values().first()
        if not colors:
          raise IndexError("Color ID doesn't exist on database.")
      else:
        colors = list(Color.objects.all().values())
      return JsonResponse(colors, safe=False)

    except IndexError as error:
      return JsonResponse({'message': str(error)}, status=404)

    except Exception as error:
      return JsonResponse({'message': str(error)}, status=500)
