from django.contrib import admin
from pieces.models import Color, Piece, PieceType

# Register your models here.
admin.site.register(Color)
admin.site.register(Piece)
admin.site.register(PieceType)