from django.urls import path, re_path

# import views
from .views.ColorsView import ColorsView
from .views.PieceTypesView import PieceTypesView
from .views.PieceView import PieceView
from .views.PredictMoveView import PredictMoveView

urlpatterns = [
    # Colors routes
    re_path(r'^colors/(?P<id>\d{1,})', ColorsView.as_view(), name='color'),
    path('colors', ColorsView.as_view(), name='colors'),

    # Types routes
    re_path(r'^types/(?P<id>\d{1,})', PieceTypesView.as_view(), name='type'),
    path('types/', PieceTypesView.as_view(), name='types'),


    # Possible moves
    re_path(
        r'moves/(?P<position>.{2,})/(?P<piece>\d+)/(?P<turns>\d+)',
        PredictMoveView.as_view(),
        name='predict_moves'
    ),

    # Pieces routes
    re_path(r"(?P<id>\d{1,})", PieceView.as_view(), name='index'),
    path("", PieceView.as_view(), name='index'),
]