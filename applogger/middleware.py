import logging
logging = logging.getLogger(__name__)

from datetime import datetime
from .models import Logger

class LoggerRequest:

  def __init__(self, get_response):
      self.get_response = get_response

  def __call__(self, request):
    response = self.get_response(request)

    '''
      The middleware will save the logger on database
    '''
    method = request.method
    route = request.path
    host = request.headers['Host']

    log = Logger()
    log.datetime = datetime.now()
    log.host = host
    log.method_request = method
    log.route = route
    log.save()

    return response
