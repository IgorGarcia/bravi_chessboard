from django.db import models

# Create your models here.


class Logger(models.Model):
  uid = models.AutoField(primary_key=True)
  datetime = models.DateTimeField(auto_now_add=True)
  host = models.CharField(max_length=255, null=False)
  method_request = models.CharField(max_length=10, null=False)
  route = models.CharField(max_length=200, null=False)