from django.forms.models import model_to_dict
from django.http.request import HttpRequest
from django.http import JsonResponse
from django.views.generic.base import View
from ..models import Color, Piece, PieceType

class PieceView(View):
  def dispatch(self, request: HttpRequest, id=None):
    self.request = request

    if self.request.method == 'POST':
      return self.store()
    return self.index(id)

  def index(self, id=None):
    try:
      if id:
        pieces = Piece.objects.filter(pk=id).values().first()
        if not pieces:
          raise IndexError("Pieces ID doesn't exist on database")
      else:
        pieces = list(Piece.objects.all().values())
      return JsonResponse(pieces, safe=False)

    except IndexError as error:
      return JsonResponse({'message': str(error)}, status=404)

    except Exception as error:
      return JsonResponse({ 'message': str(error)}, status=500)

  def store(self):
    data = self.request.POST

    #name = data['name'] if ('name' in data) else None
    type = data['type'] if ('type' in data) else None
    color = data['color'] if ('color' in data) else None

    try:
      color =  Color.objects.get(pk=color)
      type = PieceType.objects.get(pk=type)
      newPiece = Piece(color=color, type=type)
      newPiece.save()
      return JsonResponse({"id": newPiece.pk}, status=201)

    except Exception as err:
      return JsonResponse({"message":str(err)}, status=500)
