-- Set options to colors
-- I created a table to the colors to in the future make possible append a lot of board in a same game
insert into pieces_color(name, created_at) values
  ("white", DATETIME('now')),
  ("black", DATETIME('now'));

insert into pieces_pieceType(prefix, description, image, created_at) values
  ('B', 'Bishop', 'static/uploads/bishop.png', DATETIME('now')),
  ('N', 'Knight', 'static/uploads/knight.png', DATETIME('now')),
  ('K', 'King', 'static/uploads/king.png', DATETIME('now')),
  ('Q', 'Queen', 'static/uploads/queen.png', DATETIME('now')),
  ('R', 'Rook', 'static/uploads/rook.png', DATETIME('now')),
  ('P', 'Pawn', 'static/uploads/pawn.png', DATETIME('now'));