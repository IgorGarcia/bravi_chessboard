from django.db import models

class Color(models.Model):
  id = models.AutoField(primary_key=True, null=False)
  name = models.CharField(max_length=50, null=False)
  created_at = models.DateTimeField(auto_now_add=True)
  update_at = models.DateTimeField(auto_now=True, null=True)

  def __str__(self):
    return self.name


class PieceType(models.Model):
  created_at = models.DateTimeField(auto_now_add=True)
  update_at = models.DateTimeField(auto_now=True, null=True)
  prefix = models.CharField(max_length=1, null=False)
  description = models.CharField(max_length=10, null=False)
  image = models.ImageField(upload_to='static/uploads/', null=True)

  def __str__(self):
    return self.description


class Piece(models.Model):
  created_at = models.DateTimeField(auto_now_add=True)
  update_at = models.DateTimeField(auto_now=True, null=True)
  uid = models.AutoField(primary_key=True)
  # name = models.CharField(max_length=50, null=False)
  color = models.ForeignKey(Color, on_delete=models.CASCADE)
  type = models.ForeignKey(PieceType, on_delete=models.SET_NULL, null=True)

  def __str__(self):
    return self.name