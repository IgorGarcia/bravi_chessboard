from django.http.request import HttpRequest
from django.http.response import JsonResponse
from django.views.generic.base import View

from ..services.PredictMoves import predictMoves;
from ..services.AlgebraicNotationToNumber import AlgebraicNotationToNumber;
from ..services.CartesianCoordinateToAlgebraicNote import (
  CartesianCoordinateToAlgebraicNote
)

from ..models import Piece

class PredictMoveView(View):

  def dispatch(self, request: HttpRequest, position=None, piece=None, turns=2):
    self.request = request
    return self.index(position, piece, turns)


  def index(self, position, piece_id, turns=2, maxColumns=8, maxRows=8):
    piece = Piece.objects.filter(pk=piece_id).first()
    if not piece:
      return JsonResponse({ "message": "Piece not found!"}, status=404)

    initialPosition = AlgebraicNotationToNumber(position)

    moves = {
      0: [initialPosition]
    }

    if piece.type.prefix == 'N':
      moves = predictMoves(
        moves,
        maxColumns=maxColumns,
        maxRows=maxRows,
        turns=turns
      )

    for turn in moves:
      possible_moves = moves[turn]
      notations = [
        CartesianCoordinateToAlgebraicNote(item) for item in possible_moves
      ]

      moves[turn] = notations

    return JsonResponse({
      "movesByTurn": moves
    }, safe=False, status=200)