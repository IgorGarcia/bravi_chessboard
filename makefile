createsuperuser:
	python manage.py shell -c "from django.contrib.auth.models import User; \
		u, _ = User.objects.get_or_create(email='teste@teste.com'); \
		u.username = 'root'; \
		u.set_password('senha123'); \
		u.is_superuser = u.is_staff = True; \
		u.save(); \
		print('Superuser: root / senha123');"

makemigrations:
	echo 'Realizing make migrations...\n'
	python manage.py makemigrations

migrate:
	echo 'Realizing migrates...\n'
	python manage.py migrate

run:
	echo 'Starting server'
	python manage.py runserver

clear_project:
	rm -rf */migrations/0*.py
	rm -rf */*__pycache__/
	rm db.sqlite3

install_dependencies:
	pip install -r requirements.txt

pre_build:
	django-admin startproject chessboard

fixtures:
	python manage.py loaddata pieces/fixtures/types.json
	python manage.py loaddata pieces/fixtures/colors.json

build: install_dependencies makemigrations migrate createsuperuser fixtures run
