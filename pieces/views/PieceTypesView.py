from django.http.request import HttpRequest
from django.http import JsonResponse
from django.views.generic.base import View
from ..models import PieceType

class PieceTypesView(View):

  def dispatch(self, request: HttpRequest, id=None):
    self.request = request

    return self.index(id)

  def index(self, index=None):
    try:
      if index:
        typesPieces = PieceType.objects.filter(pk=index).values().first()
        if not typesPieces:
          raise IndexError("Type ID doesn't exist on database")
      else:
        typesPieces = list(PieceType.objects.all().values())
      return JsonResponse(typesPieces, safe=False)

    except IndexError as error:
      return JsonResponse({'message': str(error)}, status=404)

    except Exception as error:
      return JsonResponse({'message': str(error)}, status=500)