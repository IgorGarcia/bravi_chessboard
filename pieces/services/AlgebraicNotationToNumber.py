from re import split

def AlgebraicNotationToNumber(algebraic: str):
  letters, _ = split('\d+', algebraic)
  _, numbers = split('\D+', algebraic)

  letters = letters.upper()
  lenght_string = len(letters) - 1

  indexLetter = 0

  if lenght_string > 0:
    charcter = letters[lenght_string]
    indexLetter += (26 ** lenght_string) + (ord(charcter) - 64)
  indexLetter += ord(letters[0]) - 65

  coordinates = [int(numbers)-1, indexLetter]
  return coordinates