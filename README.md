# Instalation

![warning triangle](https://icons8.com/icon/5tH5sHqq0t2q/warning) ___Warning!___
Recommended that you create a virtual enviroment to install and run the project.

Use the followers commands:

1 - To create a virtual enviroment, you can use the command

`python -m venv [my_env]`
`cd [my_env]`

2 - Active the virtual enviroment
* Unix: `source bin/activate`
* Windows:`source Lib/activate`

3 - Desactivate the virtual enviroment

`deactivate`

____

To create all the workspace, you just need run the command in the project's root directory:
`make build`

The project will be runner at address [`localhost:8000`](http://localhost:8000)

To a better experience, run the file seed.sql in the database

---
## Routes

#### /types

__GET__
<br>
 Return all types of pieces

 - _Request_

    http://localhost:8000/piece/types/

    > ___Note:___ To get a specific type; add the ID in the end of the url.
    > http://localhost:8000/piece/types/{id}

- _Response (Example)_

      [
        {
          "id": 14,
          "created_at": "2021-09-24T00:31:43Z",
          "update_at": null,
          "prefix": "N",
          "description": "Knight",
          "image": "static/uploads/knight.png"
        },
        {
          ...
        }
      ]

#### /colors

__GET__
  <br>
  Return all colors from database

- Request
  http://localhost:8000/piece/colors
  > ___Note:___ To get a specific piece; add the ID in the end of the url.
  > http://localhost:8000/piece/colors/{id}

- Response

      [
        {
          "id": 1,
          "name": "white",
          "created_at": "2021-09-24T00:30:23Z",
          "update_at": null
        },
        {
          ...
        }
      ]


#### /piece

__GET__
<br>
Return all pieces from database

- _Request_
  http://localhost:8000/piece
  > ___Note:___ To get a specific piece; add the ID in the end of the url.
  > http://localhost:8000/piece/{id}

- _Response (Example)_

      [
        {
          created_at: "2021-09-24T00:57:17.700Z",
          update_at: "2021-09-24T00:57:17.700Z",
          uid: 1,
          color_id: 1,
          type_id: 14,
        },
        {
          ...
        }
      ]


__POST__
  <br>
  Register a new piece in the database

- Request
  http://localhost:8000/piece

  |FIELD|TYPE| Note |
  |--- |---|---|
  |type|Integer |  Type's ID from __types'__ route |
  |color|Integer | Color's ID from __colors'__ route |

- _Response (Example)_

      {
        id: 1
      }


#### /moves

__GET__
<br>
Return all the possibles moves in the next __N__ turns

- Request
  http://localhost:8000/piece/moves/{position}/{piece_id}/{turns}

  Example:
  http://localhost:8000/piece/moves/C1/1/2

- Response

      {
        "movesByTurn": {
          "0": ["C1"],
          "1": ["A2", "D3", "B3", "E2"],
          "2": ["E5", "A5", "E1", "F2", "B4", "A1", "B2", "C5", "G1", "C1", "D4", "G3", "C3", "D2", "F4"]
        }
      }

  > ___Notes___: The index represents the turn of the possibles moves